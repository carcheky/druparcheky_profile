# Druparcheky Profile

Drupal 8/9 profile to quick start develop

## Download

``
composer require carcheky/druparcheky_profile
``

or

* this option need to manually Download modules
``
composer require drupal/druparcheky_profile
``

## require

- admin_toolbar
- ctools
- devel
- layout_section_classes
- module_filter
- pathauto
- token
