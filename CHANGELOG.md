### Reverts

* Revert "fix: change source druparcheky_theme" ([6f727aa](https://gitlab.com/carcheky/druparcheky_profile/commit/6f727aabf20c0597ba16c6b7f2fb958b08c9543e))



## [2.0.5](https://gitlab.com/carcheky/druparcheky_profile/compare/v2.0.4...v2.0.5) (2020-10-24)


### Bug Fixes

* required modules scaffold ([c70d505](https://gitlab.com/carcheky/druparcheky_profile/commit/c70d50518c7be0ce307574dff95b364975940710))



## [2.0.4](https://gitlab.com/carcheky/druparcheky_profile/compare/v2.0.3...v2.0.4) (2020-10-24)


### Bug Fixes

* requirements ([4e9820f](https://gitlab.com/carcheky/druparcheky_profile/commit/4e9820f782b78b0fe6003a28f59658087c92e0e3))
* trailing comma ([1bfa860](https://gitlab.com/carcheky/druparcheky_profile/commit/1bfa8603fc5cf0f7097906325464337100d47491))



## [2.0.3](https://gitlab.com/carcheky/druparcheky_profile/compare/v2.0.2...v2.0.3) (2020-10-24)


### Bug Fixes

* change source druparcheky_theme ([7cc9a52](https://gitlab.com/carcheky/druparcheky_profile/commit/7cc9a5273ac3663ef653fd620b05e634d397a87a))



## [2.0.2](https://gitlab.com/carcheky/druparcheky_profile/compare/v2.0.1...v2.0.2) (2020-10-24)


### Bug Fixes

* required   - layout_discovery ([a125f54](https://gitlab.com/carcheky/druparcheky_profile/commit/a125f54f9a58c2fa613afd766f19b6ac9653dae3))



## [2.0.1](https://gitlab.com/carcheky/druparcheky_profile/compare/v2.0.0...v2.0.1) (2020-10-24)


### Bug Fixes

* core_version_requirement ([56c3b82](https://gitlab.com/carcheky/druparcheky_profile/commit/56c3b8280fa1680873d7ec38f5452459732fcce6))



# [2.0.0](https://gitlab.com/carcheky/druparcheky_profile/compare/v1.1.4...v2.0.0) (2020-10-24)


### Continuous Integration

* BREAKING CHANGE ([09e71ba](https://gitlab.com/carcheky/druparcheky_profile/commit/09e71ba516530dcb06ea355301a588ac8ef4af6d))


### BREAKING CHANGES

* NEW VERSION



## [1.1.4](https://gitlab.com/carcheky/druparcheky_profile/compare/v1.1.3...v1.1.4) (2020-10-24)


### Bug Fixes

* **ci:** push 2 drupal ([971a551](https://gitlab.com/carcheky/druparcheky_profile/commit/971a551995a5dd6a9c4c738a92245e7b454b3a3a))



## [1.1.3](https://gitlab.com/carcheky/druparcheky_profile/compare/v1.1.2...v1.1.3) (2020-10-24)


### Bug Fixes

* **ci:** push 2 drupal ([fcf4d0c](https://gitlab.com/carcheky/druparcheky_profile/commit/fcf4d0c0f9e8bb406b29548e457bdcf74fee2ba0))


### Reverts

* Revert "layout builder enabled by default" ([14eb8b4](https://gitlab.com/carcheky/druparcheky_profile/commit/14eb8b4e6d9020af04d342422f92ff7692400436))



## [1.1.2](https://gitlab.com/carcheky/druparcheky_profile/compare/v1.1.1...v1.1.2) (2020-10-08)


### Bug Fixes

* core version requirement ([cb587be](https://gitlab.com/carcheky/druparcheky_profile/commit/cb587be2737580591b2d359901056a7829aa57b3))



## [1.1.1](https://gitlab.com/carcheky/druparcheky_profile/compare/v1.1.0...v1.1.1) (2020-09-16)


### Bug Fixes

* a fix ([1064051](https://gitlab.com/carcheky/druparcheky_profile/commit/1064051493b680c77d8f43503612dd18373b79d3))
* ci ([f90705c](https://gitlab.com/carcheky/druparcheky_profile/commit/f90705c47970b1aa39953b506cee19fff863a19f))
* ci ([c63a538](https://gitlab.com/carcheky/druparcheky_profile/commit/c63a5381fd03f4468b2717732734d952dce97a0d))


### Features

* branches ([27f1823](https://gitlab.com/carcheky/druparcheky_profile/commit/27f1823a5b31d51596e829165bf68c1b0fff89f0))
* ci ([33de6a1](https://gitlab.com/carcheky/druparcheky_profile/commit/33de6a120d092943741b8b92984b53ea76006880))
* d9 ([e1b5a88](https://gitlab.com/carcheky/druparcheky_profile/commit/e1b5a88960084b52b2c130e21a65b17389b37030))
* prerelease ([987dbb7](https://gitlab.com/carcheky/druparcheky_profile/commit/987dbb7e2a1b829dc13b5eb8a18ba5c6182288c1))
* **workspace:** workspace ([66fa309](https://gitlab.com/carcheky/druparcheky_profile/commit/66fa309be97f32ee5efcd52be45d188b48bfdff3))
* removed imce ([174f58c](https://gitlab.com/carcheky/druparcheky_profile/commit/174f58c195ca01825ccc51839b253e83ddab90c8))
* removed version ([8e42bdb](https://gitlab.com/carcheky/druparcheky_profile/commit/8e42bdb6d447c9f076f300c189b2f5734d94dc7c))
* **REMOVED IMCE:** REMOVED IMCE ([4238cd5](https://gitlab.com/carcheky/druparcheky_profile/commit/4238cd51ab1dcddd77e8106fce5e61f1bc2839b8))



## [1.0.9](https://gitlab.com/carcheky/druparcheky_profile/compare/1.0.8...v1.0.9) (2020-06-22)



## [1.0.8](https://gitlab.com/carcheky/druparcheky_profile/compare/1.0.7...1.0.8) (2020-06-22)



## [1.0.5](https://gitlab.com/carcheky/druparcheky_profile/compare/1.0.4...1.0.5) (2020-06-05)



## [1.0.4](https://gitlab.com/carcheky/druparcheky_profile/compare/1.0.3...1.0.4) (2020-06-04)



## [1.0.3](https://gitlab.com/carcheky/druparcheky_profile/compare/1.0.2...1.0.3) (2020-06-03)



## [1.0.2](https://gitlab.com/carcheky/druparcheky_profile/compare/1.0.1...1.0.2) (2020-05-30)



## [1.0.1](https://gitlab.com/carcheky/druparcheky_profile/compare/1.0.0...1.0.1) (2020-05-19)



# 1.0.0 (2020-05-18)


